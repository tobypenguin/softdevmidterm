/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softdevmidterm;

import java.io.Serializable;

/**
 *
 * @author TUFGaming
 */
public class Product implements Serializable {

    private String productID;
    private String productName;
    private String productBrand;
    private double productPrice;
    private int productAmount;

    public Product(String productID, String productName, String productBrand, double productPrice, int productAmount) {
        this.productID = productID;
        this.productName = productName;
        this.productBrand = productBrand;
        this.productPrice = productPrice;
        this.productAmount = productAmount;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    @Override
    public String toString() {
        return "ID : " + productID + ", Name : " + productName + ", Brand : " + productBrand + ", Price : " + productPrice + ", Amount : " + productAmount;
    }

}
